import XCTest

import SPMGameTests

var tests = [XCTestCaseEntry]()
tests += SPMGameTests.allTests()
XCTMain(tests)
